(* 
	Section 2 Code Review 
	Review of Pset 1
 *)

(** Technicall calculating length and sum at the same time 
 *  is faster, but it's also more confusing, and tail recursion
 *   matters so much more if we're talking about run time. 
 *)

(** The second version declares all the helpers inline, meaning that they aren't 
 *  available for other functions. Helpful for keeping things relevant only
 *  where they should be, but if you might want to use any of thse again
 *  you'd want to declare it not within variance.
 *)

open List;; (** Let's you use List.* functions without typing List. 
             *  e.g. length [1;2;3] instead of List.length [1;2;3].
             *  I don't ever use it, since I like being able to easily
             *  see where all my functions are coming from 
             *)
