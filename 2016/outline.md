# Stefan's CS 51 Code Reviews

## 2/7

### Git
- [interactive tutorial](https://bitbucket.org/srajkovic/cs51-git-tutorial)
- GitHub has an
  [awesome in browser example!](https://try.github.io/levels/1/challenges/1)
- `.gitignore` to ignore certain files. Theres on in the base of this repo,
  that ignore OCaml build artifacts

### Text Editor
- command line (primarily, also available as GUI)
  - emacs
    - I like it because its super extensible
      - syntax highlighting
      - on the fly compiling, showing errors in red
      - can send lines directly to toplevel without copy and paste
  - vim
    - super fast movement, using keystrokes strung together
- GUI
  - TextEdit
    - messes up quotes sometimes
    - no syntax highlighting
  - Sublime
    - decent amount of plugins
  - Atom
    - open sourced by GitHub, decent plugins I think
  - VSCode
    - open sourced by Microsoft, no idea of plugin landscape

### Lab0 review
See `1/cr1_code_examples.ml`. My comments are in `(** *)`,
notice the double asterisk

### Other notes:
- Options
  - C malloc returns NULL on failure or an address on success
  - OCaml malloc would return `None` on failure, or `Some address` on success
  - Enforces checking for these error conditions in type system
  ```
  match x with
  | Some v -> do_whatever
  | None -> do_something_else
  ```
- Lists

  ```
  match lst with
  | hd :: hd2 :: tl -> do_something
  ```
  - if you use it you need to include a case that looks like hd :: [],
    to handle 2 element lists
- matching on multiple values:

  ```
  match x, y with
  | [], [] -> ...
  | hdx :: tlx, [] -> ..
  ```
- Tuples
  - assume x has type (`int list * int list`)
  - `a,b = x` (now a is the first `int list`, and b is the second)
- Compiling and Running
  - from the command line, run `make` (in your directory with `ps1.ml`)
    to compile your work
  - run `./ps1.byte` to run it
  - Then run `./ps1_tests.byte` to run the tests (that you should be writing!)

## 2/14
	
### Lab2 review
- curry vs uncurry, and why do we care?
  - curried
    - `f x y`
    - partial application
  - uncurried
    - `f (x,y)`
    - efficiency
- records
  - like structs in C.

### PS1 review
see `2/cr2_code_samples.ml`. My comments are in `(** *)`.

### PS2 prep
- `mapfold.ml`
  - HOF
    - `List.fold_right`
    - `List.fold_left`
    - `List.map`
    - `List.filter`
    - A bunch of others too, see the docs (linked below)
  - Also use other like `List.length`, but don't use `List.concat` for
    the one that wants you to implement `List.concat`.
  - `ListLabeled` works the same way, but with labeled arguments should that
    suit you
  - I've included `2/list.ml` which is the actual List module from the latest
    Ocaml release.
    - I don't think there's anything in there that you guys can't understand
      in terms of syntax. 
- `expression.ml`
  - Algebraic Data Types
    - definition
    ```
    type expression =
      | Var
      | Num of float
      ...
    ```
    - match cases
    ```
      match e with
      | Var ->
      | Num x ->
      ...
    ```
  - built-in functions
    - [Docs](http://caml.inria.fr/pub/docs/manual-ocaml/libref/index.html)
      - If something says "since 4.02.3", ignore it, since we're using 4.02.2
    - anything in Pervasives you can reference just by the name after the dot.
      - e.g. `floor`, `sin`, etc.
      - imagine an `open Pervasives` at the top of every file.
        (see code_review.ml for description of open)
  - `Makefile`
    - let's you write the compilation command once, and run it more easily
      from the command line.
    - See your `ps1` repo for an example, which should be enough for you to
      write one for ps2.
      - rather than targets `ps1` and `ps1_tests`, you might want
      	`mapfold` and `mapfold_tests`
    - Ask me if you have any questions about them!
    - They'll save you so much time, if you use them, and run your code and
      tests like that, rather than copy and pasting

## 2/21

### Lab3 review
- algebraic data types (tuple = product, variants = sum)
- record syntax (similar to structs in C)

### PS2 review
- `mapfold.ml`
  - `deoptionalize`
    - many different ways of implementing this
    - most people end up with filter + map, but have to raise an exception
    - using fold to handle filter and map at the same time is the best solution
  - `concat`, `sum`, `sum_rows`
    - don't write wrapper functions for binary infix operators like
      `(fun x y -> x @ y)`. Instead use the prefix form: `(@)`.
  - `num_occurs`
    - `fold_right` let's you do it in one pass
    - taking the sum and dividing is overkill, using `List.length`
      would be much better.
  - `filter_range`
    - preferable to just filter once, checking both parts of the range at once
- `expression.ml`
  - `contains_var`
    - don't use if/then/else if you're just returning a boolean, use || or &&.
  - `evaluate`
    - could simplify with this:
    ```
    let f = match binop with
    	    | Add -> (+.)
            | Sub -> (-.)
            ... in
    f (evaluate exp1 x) (evaluate exp2 x)
    ```
    - make sure to test every code path
      - better to test just one code path at a time, rather than a huge test
      - more granularity makes it easier to identify the point of error
 - overall takeaway should be that defining your type system properly abstracts
   work away from you as a developer.

### PS3 preview
- `bignums`
  - OCaml ints are limited in size. What if we want to operations
    with huge numbers?
  - arbitrary precision arithmetic
  - think back to your grade school arithmetic
  - make sure your code works with any arbitrary base

## 2/28

### PS3 review
- `times` was rough
  - `times1`
    - move specific helpers into definition of `times`
    - Int.pow can overflow
    - this implementation is wrong
  - `times2`
    - remove placeholders
    - simplify if then in `part`, since values are the same
    - `part`, `sum`, `exec`, and `zeroes_lst` can all be rolled together.

### PS4 prep
- Modules, Interfaces, Functors?
- Priority Queues
  - ordered by priority, first in first out (FIFO), for ties
  - ex: boarding a plane, priority = class
  - backing data structure is important
    - list
      - add: `O(n)`
      - min: `O(1)`
    - binary tree
      - add: `O(log n)`? Also potentially `O(n)`
      - min: `O(log n)`? Also potentially `O(n)`
    - binary min-heap
      - add: `O(log n)`
      - min: `O(log n)`
    - fibonacci heap (definitely not required)
      - add: `O(1)`
      - min: `O(log n)`
  - Binary Heap
    - _weak/shape_ invariant: all levels are full except the last one
    - _strong/heap_ invariant: every node is less/greater than it's children
    - ex: We have `10, 6, 6, 5, 11, 1`, all to add to the heap.
    - Step 1 `insert 10`:
      ![Step 1](stage1.png)
    - Step 2 `insert 6`:
      ![Step 2](stage2.png)
    - Step 3 `insert 6`:
      ![Step 3](stage3.png)
    - Step 4 `insert 5`:
      ![Step 4](stage4.png)
    - Step 5 `insert 11`:
      ![Step 5](stage5.png)
    - Step 6 `insert 1`:
      ![Step 6](stage6.png)    
    - Step 7 `remove`:
      ![Step 7](stage7.png)
- `insert`:      
  1. Add the element to the bottom level of the heap.
  2. Compare the added element with its parent;
     if they are in the correct order, stop.
  3. If not, swap the element with its parent and return to the previous step.
- `remove`:
  1. Replace the root of the heap with the last element on the last level.
  2. Compare the new root with its children;
     if they are in the correct order, stop.
  3. If not, swap the element with the small child,
     and return to the previous step.

### Unit Tests
- prevent regressions when updating code
- catch bugs (I've seen tests that would have caught correctness bugs in a
  students code, had they run the tests)

### Honor Code
- follow it, nobody wants to go to/send anyone to the Ad Board
- we do have an automated system for checking for plagiarism

## 3/6

### Midterm Review
- type system
- HOF
- design and use of ADT
- design and use of modules
- recursion
- substitution model
- NOT recurrence relations and runtime

## 3/20

### Midterm Postmortem
- module usage (problem 12)
  - ran out of time?
  - just read the signature given for module type IMAGING,
    and use things in there
- map vs. fold_right
  - `map`'s type is `('a -> 'b) -> 'a list -> 'b list`
  - `fold_right`'s type is `('a -> 'b -> 'b) -> 'a list -> 'b -> 'b`
  - notice the last element (the return value). `map` has a `'b list`, while
    fold_right has a `'b`. `map`'s return type isn't as general as
    `fold_right`'s, so we can't use `map` to implement `fold_right`
- `second`
  - Don't just use compose with the identity or something
  - Think about how you would implement second recursively? You'd get the
    tail of the list, than take the head of that most likely. That's the
    composition of List.tl and List.hd, `compose List.tl List.hd`.
- 2.3 
  - Didn't type check:
  ```
  let f x =
  match x with (* x is a 'a list based on the match possibilities *)
  | [] -> x (* return a 'a list *)
  | h :: t -> h (* return a 'a *)
  (* the return value types don't align on each match, doesn't type check *)
  ```
- 1.3
  - just needed a function that takes any list and converts it to a bool list.
  - simple example: `List.map (fun x -> true)`
- 1.4
  - this was tricky, since it was easy to have return value `'c` instead of `'a`
  ```
  let f g a b =
  if g (a, b) = a then a else a in
  (* g (a, b) was common *)
  in f;;
  ```
- `better`
  - can be simple if you define your types well (often the case in OCaml)
  - `let better card1 card2 = card1 < card2` if you had this type definition:
  ```
  type suit = Spades | Diamonds ;;
  type value = King | Queen | Jack ;;
  type card = {suit: suit; value: value} ;;
  (* or type card = suit * value *)
  ```
  
### Moogle prep
- start early
- read code (efficiently)
- rule of thumb: always read anything that ends in `.mli` or (`.h` in C, etc.)
- I've extracted all the important signatures into `cr6_code_examples.ml`,
  you'll be using those functions a lot.
- the timing part of the pset is up to you to design! Feel free to run designs
  by  me or other TFs
  - usually benchmarking runs more than once
  - Look at the Sys module for things that might be helpful
  - report:
    - what: your results
    - how: how you get them
    - why: why did you get those results?

## 3/27

### PS4 Review
- `search`
  - `EQUAL != (=)`
    - List.mem was the easiest way of doing this
- `add` and `take`
  - two possible implementations using lists
    - list sorted in descending order
      - add puts it in wherever it fits, EQUAL ahead (`O(n)`)
      - take reverses, and pops off the head (`O(n)`)
    - list sorted in ascending order
      - add puts it in wherever it fits, EQUAL behind (`O(n)`)
      - take removes the first element (`O(1)`)
    - the second solution is better since we save time on `take`
	
### Runtime
- Asymptotic notation
  - `O()` AKA Big-O
    - If we say `f` is `O(g)` where `f` and `g` are functions of `n`, then
      for some "large enough" `n`, and some *constant* coefficient `m`,
      `f(n) <= m * g(n)`.
    - This is a big range, so it's easy to say that some computation is say
      `O(n^n)`, so when we look at Big-O we're assuming we want the tightest
      provable bound
    - Helpful properties
      - constants don't matter: `3n^4 + 2` is `O(n^4)
      -	only the largest term matters: `n^2 + n` is `O(n^2)`
    - Helpful comparisons
      - Logarithmic << Polynomial << Exponential << Factorial << PolyExponential
      	(that last ones not a word but I don't know if a word exists for that)
      - `log n << n^c << c^n << n! << n^n`
  - Recurrence relations
    - How much time does each function call take, in terms of what it calls?
    - `T(n)` in terms of `T(n-1)` (or `T(n/2)`, etc.)
    - Recurrence relations can usually lead you to Big-O
      - because of the nature of recursion, think logarithmic or linear first
	  
### Refs
  - a lot like pointers were in C, except with a fraction of the functionality
  - use
    - type: `int ref`
    - initialization: `let x = ref 3...`
    - updating: `x := 4`
    - reading: `!x`
  - Not commonly used in idiomatic OCaml
    - in Jane Street's Core (~40,000 lines), there are 51 lines that use refs.
	
### Moogle
  - `crawl`
    - Use `ListSet` and `WordDict`
    - Most code in the pset is here
  - `DictSet`
    - mostly comprised of creating a Dict module and using it
  - Timing
    - you're free to do this however you want, its intentionally underspecified

## 4/3

### Moogle Review
  - `crawl`
	- some people had SO issues, and others `EMFILE`. We're not exactly sure why
	  for `EMFILE`, but we'll look into it. Let me know if you were getting it.
    - A lot of opportunities for using `fold_left`, which can help significantly
	  simplify the code (how the staff solution gets it to 23 lines).
  - `DictSet`
	- using `D` module to implement `SET` using a dictionary as your DS
	- Creating a `DICT_ARG` module to pass in
	  - Mostly using functions from `C : COMPARABLE`
    - Using `D` to implement the `SET` functions
	  - `intersect` and `union` both can be done in terms of `D.foldl`
	  - reversing argument order is easy with anonymous functions
		- say you have `f : 'a -> int -> 'b` and it does exactly what you
		  need for `g`, except `g` has signature `int -> 'a -> 'b`. Just use
		  `let g = (fun i a -> f a i)`.
  - Timing
	- variety of implementations from totally manual, to modifying main to
	  run the `crawl` and queries many times and getting times from each of
	  those, to having test code that runs crawl many times with different `n`
	  and averages them out.
    - should have seen that as `n` grows, `DictSet` is asymptotically
	  faster than `ListSet`. Why might `ListSet` be better for small `n`?
	  
### Conceptual Review
  - infinite streams are helpful in terms of modeling a lot real world
	applications, particularly events, e.g. tweets, math series, keyboard
	strokes, etc.
  - memoization is when computation results are stored for later reuse.
	- what's required for this to be possible? No side effects!
	- can dramatically improve run time of recursive functions
  - 

### TreeStreams
  - `ref`s
	- mutable lists, cycle detection, and cycle destruction
  - infinite `stream`s and `tree`s
	- lazy evaluation means that nothing is actually computed until the result
	  is required.
    - this let's us have infinite data structures without infinite recursion
	- `average` and `aitkens` are just modifying streams, don't require any
	  knowledge of math or Taylor series
    - `tree`s are a little bit more complicated. They aren't necessarily
	  binary, and you shouldn't treat them as such
		- the exception is the specific ones you're asked to implement
		  which are binary

### Moosic
  - converting a DS for a single line of music seen as
	(notes/rests + durations and volumes) into MIDI events, which are
	starts and stops of particular pitches, relative to the previous event
  - merging two streams of MIDI events being careful of shifting time.
  - creating beautiful moosic
  
